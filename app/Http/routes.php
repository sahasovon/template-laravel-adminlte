<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', ['as' => 'home', 'uses' => function () {
    return view('welcome');
}]);

Route::auth();

Route::get('/home', 'HomeController@index');

/* Admin Section */
Route::group([
    'prefix' => 'admin',
    'namespace' => 'Admin',
    'as' => 'admin::'
], function()
{
    Route::get('/', ['as' => 'login.form', 'uses' => 'AuthController@getLoginForm']);
    Route::post('login', ['as' => 'login.check', 'uses' => 'AuthController@login']);

    Route::group([
        'middleware' => ['auth-admin']
    ], function()
    {
        Route::get('dashboard', ['as' => 'dashboard', 'uses' => 'DashboardController@index']);

        /* Settings- change email and password */
        Route::get('settings', ['as' => 'settings', 'uses' => 'DashboardController@editSettings']);
        Route::patch('settings', ['as' => 'settings.update', 'uses' => 'DashboardController@updateSettings']);
    });
});

